import os
from loggers.file_handler import ORBFileHandler
from kivy.logger import Logger, console as console_handler
from kivy import Config
# create and set default values for handlers

debug_file_handler = ORBFileHandler()
debug_file_handler.setLevel('INFO')

error_file_handler = ORBFileHandler()
error_file_handler.setLevel('WARNING')


def configure_loggers():
    Config.set('kivy', 'log_level', 'debug')
    log_dir = Config.getdefault('logs', 'log_dir', '~/.orb_logs')
    try:
        debug_mode = Config.getboolean('general', 'debug')
    except ValueError:
        debug_mode = False

    full_logs_dir = os.path.expanduser(log_dir)

    error_file_handler.log_dir = os.path.join(full_logs_dir, 'errors')
    debug_file_handler.log_dir = os.path.join(full_logs_dir, 'info')
    # error_file_handler.emit_history()
    # error_file_handler.emit_history()

    if debug_mode:
        debug_file_handler.setLevel('DEBUG')
        console_handler.setLevel('DEBUG')

    Logger.addHandler(debug_file_handler)
    Logger.addHandler(error_file_handler)
