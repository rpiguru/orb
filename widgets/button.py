import os
from kivy.animation import Animation
from kivy.app import App
from assets.styles import defaultstyle
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from base import AnimatedWidget, ClickableWidget
from kivy.uix.widget import Widget
from kivy.vector import Vector

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'button.kv'))


class ORBButton(Button, AnimatedWidget):
    button_type = StringProperty('blue')

    def set_white_color(self):
        self.color = defaultstyle.BACKGROUND_COLOR_DARK
        self.background_normal = 'assets/images/button/white_button.png'
        self.background_down = 'assets/images/button/white_button_pressed.png'

    def set_default_color(self):
        self.color = (1, 1, 1, 1)
        self.background_normal = 'assets/images/button/button.png'
        self.background_down = 'assets/images/button/button_pressed.png'

    def on_button_type(self, obj, val):
        if self.button_type == 'blue':
            self.set_default_color()
        else:
            self.set_white_color()


class WhiteButton(ORBButton, AnimatedWidget):
    pass


class QuietButton(Button, AnimatedWidget, ClickableWidget):
    def __init__(self, **kwargs):
        super(QuietButton, self).__init__(**kwargs)
        self.register_event_type('on_release')

    def on_touch_up(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.dispatch('on_release')

    def on_touch_move(self, touch):
        pass

    def on_release(self):
        pass


class CircularButton(ButtonBehavior, Widget):

    text = StringProperty('')
    text_color = ListProperty([.1, .1, .1, 1])
    background_color = ListProperty(defaultstyle.COLOR_2)

    def collide_point(self, x, y):
        return Vector(x, y).distance(self.center) <= self.width / 2


class ORBTabButton(Button, AnimatedWidget):

    _line_width = NumericProperty(0)

    def __init__(self, **kwargs):
        super(ORBTabButton, self).__init__(**kwargs)
        self.on_tabbed(False)

    def on_tabbed(self, val):
        if val:
            self.background_color = defaultstyle.BACKGROUND_COLOR_LIGHT
            Animation(_line_width=self.width, duration=.2, t='out_quad').start(self)
        else:
            self.background_color = defaultstyle.BACKGROUND_COLOR_LIGHT_LIGHT
            Animation(_line_width=0, duration=.2, t='out_quad').start(self)


Factory.register('ORBButton', cls=ORBButton)
Factory.register('WhiteButton', cls=WhiteButton)
Factory.register('QuietButton', cls=WhiteButton)
Factory.register('CircularButton', cls=CircularButton)
