import os
import re

import sys
from kivy.animation import Animation
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ReferenceListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivymd.textfields import MDTextField
from widgets.dialog import FolderChooserDialog

from .base import DefaultInput, LabeledInputBase, AnimatedWidget


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'input.kv'))


class ORBTextInput(TextInput, DefaultInput, AnimatedWidget):

    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        pass

    def mark_as_error(self):
        pass

    max_length = NumericProperty(100)

    def insert_text(self, substring, from_undo=False):
        super(ORBTextInput, self).insert_text(substring, from_undo)
        self.text = self.text.lstrip()[:self.max_length]

    def on_text_validate(self):
        next_widget = self._get_focus_next('focus_next')
        if next_widget is not None:
            next_widget.focus = True

    def _get_value(self):
        return self.text

    def set_value(self, value):
        self.text = value


class LabeledTextInputBase(LabeledInputBase):
    hint_text = StringProperty('')
    password = BooleanProperty(False)


class LabeledTextInput(LabeledTextInputBase):
    pass


class ORBDateInput(ORBTextInput):
    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        pass

    def mark_as_error(self):
        pass

    hint_text = StringProperty('MM/DD/YYYY')

    def insert_text(self, substring, from_undo=False):
        """
        Format date input
        :param substring:
        :param from_undo:
        :return:
        """
        super(ORBDateInput, self).insert_text(substring, from_undo)
        digits = re.findall(r'\d+', self.text)
        digits = ''.join(digits)
        formated_string = ''

        i = 0
        for c in self.hint_text:
            if str.isalnum(c):
                if i == len(digits):
                    break
                formated_string += digits[i]
                i += 1
            else:
                formated_string += c
        self.text = formated_string
        self.cursor = (len(formated_string), 0)

    def _get_value(self):
        data = re.findall(r'\d+', self.text)
        # convert date from mm-dd-yyyy to yyyy-mm-dd format
        date = []
        if len(data) > 0:
            date.append(data[0])

        if len(data) > 1:
            date.append(data[1])
            date[0], date[1] = date[1], date[0]

        if len(data) > 2:
            date.append(data[2])
        return '-'.join(date[::-1]) if date else ''


class LabeledDateInput(LabeledTextInputBase):
    hint_text = StringProperty('MM/DD/YYYY')

    def _get_value(self):  # TODO looks like unreachable code
        data = super(LabeledDateInput, self)._get_value()
        data = re.findall(r'\d+', data)
        # convert date from mm-dd-yyyy to yyyy-mm-dd format
        date = []
        if len(data) > 0:
            date.append(data[0])

        if len(data) > 1:
            date.append(data[1])
            date[0], date[1] = date[1], date[0]

        if len(data) > 2:
            date.append(data[2])

        return '-'.join(date[::-1]) if date else ''


class LabeledIPInput(LabeledTextInputBase):

    def custom_error_check(self):
        return self.ids.input.custom_error_check()


class ORBLabeledTextInput(BoxLayout):
    label = StringProperty('')
    hint_text = StringProperty('')
    password = BooleanProperty(False)
    text = StringProperty('')

    def __init__(self, **kwargs):
        super(ORBLabeledTextInput, self).__init__(**kwargs)

    def set_value(self, _val):
        self.ids.input.set_value(_val)

    def get_value(self):
        return self.ids.input.get_value()


class FolderPathInput(ORBTextInput):

    pHint_x = NumericProperty(.5)
    pHint_y = NumericProperty(.7)
    pHint = ReferenceListProperty(pHint_x, pHint_y)

    def __init__(self, **kwargs):
        super(FolderPathInput, self).__init__(**kwargs)
        self.register_event_type('on_change')

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            popup = FolderChooserDialog(path=self.text, size_hint=self.pHint)
            popup.bind(on_confirm=self.on_confirm_path)
            popup.open()
        super(FolderPathInput, self).on_touch_down(touch)

    def on_confirm_path(self, *args):
        self.text = args[1]
        self.dispatch('on_change')

    def on_change(self):
        pass


class ORBMDTextField(MDTextField):

    _hint_lbl_font_size = NumericProperty(24)
    _hint_y = NumericProperty(38)

    def on_focus(self, *args):
        Animation.cancel_all(self, '_line_width', '_hint_y',
                             '_hint_lbl_font_size')
        if self.max_text_length is None:
            max_text_length = sys.maxsize
        else:
            max_text_length = self.max_text_length
        if len(self.text) > max_text_length or all([self.required, len(self.text) == 0, self.has_had_text]):
            self._text_len_error = True
        if self.error or all([self.max_text_length is not None and len(self.text) > self.max_text_length]):
            has_error = True
        else:
            if all([self.required, len(self.text) == 0, self.has_had_text]):
                has_error = True
            else:
                has_error = False

        if self.focus:
            self.has_had_text = True
            Animation.cancel_all(self, '_line_width', '_hint_y',
                                 '_hint_lbl_font_size')
            if len(self.text) == 0:
                Animation(_hint_y=20,
                          _hint_lbl_font_size=16, duration=.2,
                          t='out_quad').start(self)
            Animation(_line_width=self.width, duration=.2, t='out_quad').start(self)
            if has_error:
                Animation(duration=.2, _current_hint_text_color=self.error_color,
                          _current_right_lbl_color=self.error_color,
                          _current_line_color=self.error_color).start(self)
                if self.helper_text_mode == "on_error" and (self.error or self._text_len_error):
                    Animation(duration=.2, _current_error_color=self.error_color).start(self)
                elif self.helper_text_mode == "on_error" and not self.error and not self._text_len_error:
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)
                elif self.helper_text_mode == "persistent":
                    Animation(duration=.2, _current_error_color=self.theme_cls.disabled_hint_text_color).start(self)
                elif self.helper_text_mode == "on_focus":
                    Animation(duration=.2, _current_error_color=self.theme_cls.disabled_hint_text_color).start(self)
            else:
                Animation(duration=.2, _current_hint_text_color=self.line_color_focus,
                          _current_right_lbl_color=self.theme_cls.disabled_hint_text_color).start(self)
                if self.helper_text_mode == "on_error":
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)
                if self.helper_text_mode == "persistent":
                    Animation(duration=.2, _current_error_color=self.theme_cls.disabled_hint_text_color).start(self)
                elif self.helper_text_mode == "on_focus":
                    Animation(duration=.2, _current_error_color=self.theme_cls.disabled_hint_text_color).start(self)
        else:
            if len(self.text) == 0:
                Animation(_hint_y=44,
                          _hint_lbl_font_size=24, duration=.2,
                          t='out_quad').start(self)
            if has_error:
                Animation(duration=.2, _current_line_color=self.error_color,
                          _current_hint_text_color=self.error_color,
                          _current_right_lbl_color=self.error_color).start(self)
                if self.helper_text_mode == "on_error" and (self.error or self._text_len_error):
                    Animation(duration=.2, _current_error_color=self.error_color).start(self)
                elif self.helper_text_mode == "on_error" and not self.error and not self._text_len_error:
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)
                elif self.helper_text_mode == "persistent":
                    Animation(duration=.2, _current_error_color=self.theme_cls.disabled_hint_text_color).start(self)
                elif self.helper_text_mode == "on_focus":
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)
            else:
                Animation(duration=.2, _current_line_color=self.line_color_focus,
                          _current_hint_text_color=self.theme_cls.disabled_hint_text_color,
                          _current_right_lbl_color=(0, 0, 0, 0)).start(self)
                if self.helper_text_mode == "on_error":
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)
                elif self.helper_text_mode == "persistent":
                    Animation(duration=.2, _current_error_color=self.theme_cls.disabled_hint_text_color).start(self)
                elif self.helper_text_mode == "on_focus":
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)

                Animation(_line_width=0, duration=.2, t='out_quad').start(self)

    def on_text(self, instance, text):
        if len(text) > 0:
            self.has_had_text = True
        if self.max_text_length is not None:
            self._right_msg_lbl.text = "{}/{}".format(len(text), self.max_text_length)
            max_text_length = self.max_text_length
        else:
            max_text_length = sys.maxsize
        if len(text) > max_text_length or all([self.required, len(self.text) == 0, self.has_had_text]):
            self._text_len_error = True
        else:
            self._text_len_error = False
        if self.error or self._text_len_error:
            if self.focus:
                Animation(duration=.2, _current_hint_text_color=self.error_color,
                          _current_line_color=self.error_color).start(self)
                if self.helper_text_mode == "on_error" and (self.error or self._text_len_error):
                    Animation(duration=.2, _current_error_color=self.error_color).start(self)
                if self._text_len_error:
                    Animation(duration=.2, _current_right_lbl_color=self.error_color).start(self)
        else:
            if self.focus:
                Animation(duration=.2, _current_right_lbl_color=self.theme_cls.disabled_hint_text_color).start(self)
                Animation(duration=.2, _current_hint_text_color=self.line_color_focus,
                          _current_line_color=self.line_color_focus).start(self)
                if self.helper_text_mode == "on_error":
                    Animation(duration=.2, _current_error_color=(0, 0, 0, 0)).start(self)
        if len(self.text) != 0 and not self.focus:
            self._hint_y = 20
            self._hint_lbl_font_size = 16


class ORBIPInput(ORBMDTextField):

    key = StringProperty('')

    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        errors = []
        if len(self.text) == 0:
            return errors
        for val in self.text.split('.'):
            try:
                if int(val) > 255:
                    errors.append('Invalid IP address value')
                    break
            except ValueError:
                errors.append('Invalid IP address value')
                break

        if len(self.text.split('.')) != 4:
            errors.append('Invalid structure')

        return errors

    def mark_as_error(self):
        pass

    hint_text = StringProperty('255.255.255.255')
    max_length = NumericProperty(15)

    def insert_text(self, substring, from_undo=False):
        """
        Format date input
        :param substring:
        :param from_undo:
        :return:
        """
        str_in = substring.encode('ascii', 'ignore')
        if str_in == '.':
            if self.text.count('.') >= 3:
                return
            elif str(self.text[-1]) != '.':
                super(ORBIPInput, self).insert_text(substring, from_undo)
                return
            else:
                return
        elif not str.isdigit(str_in):
            return
        elif len(self.text.split('.')[-1]) == 3:
            return
        super(ORBIPInput, self).insert_text(substring, from_undo)
        self.cursor = (len(self.text), 0)

    def get_value(self):
        return {self.key: self.text}


Factory.register('ORBTextInput', cls=ORBTextInput)
Factory.register('LabeledTextInput', cls=LabeledTextInput)
Factory.register('ORBDateInput', cls=ORBDateInput)
Factory.register('LabeledDateInput', cls=LabeledDateInput)
Factory.register('ORBIPInput', cls=ORBIPInput)
Factory.register('LabeledIPInput', cls=LabeledIPInput)
Factory.register('ORBLabeledTextInput', cls=ORBLabeledTextInput)
Factory.register('ORBMDTextField', cls=ORBMDTextField)
