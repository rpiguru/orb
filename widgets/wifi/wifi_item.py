import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.list import TwoLineAvatarIconListItem, BaseListItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'wifi_item.kv'))


class WiFiItem(BoxLayout):

    text = StringProperty('')
    signal_percentage = NumericProperty(0)
    icon_strength = NumericProperty(0)
    connected = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(WiFiItem, self).__init__(**kwargs)
        self.register_event_type('on_press')
        if self.signal_percentage == 100:       # avoid 100% due to the file name
            self.signal_percentage = 99
        self.icon_strength = int(self.signal_percentage / 20)
        if self.connected:
            self.ids.btn_connect.text = 'Info'
            self.text += " - [i][color=aaaaaa]Connected[/color][/i]"
            self.ids.img_chk.opacity = 1

    def on_btn(self):
        self.dispatch('on_press')

    def on_press(self):
        pass


class MDWiFiItem(TwoLineAvatarIconListItem):

    _txt_top_pad = NumericProperty(12)
    _txt_bot_pad = NumericProperty(8)

    def __init__(self, **kwargs):
        super(BaseListItem, self).__init__(**kwargs)
        self.height = 72


if __name__ == '__main__':
    from kivy.base import runTouchApp
    runTouchApp(WiFiItem())
