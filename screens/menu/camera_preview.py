import os

from kivy.lang import Builder
from kivymd.accordion import MDAccordionItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'camera_preview.kv'))


class CameraPreview(MDAccordionItem):

    def __init__(self, **kwargs):
        super(CameraPreview, self).__init__(**kwargs)

    def on_collapse(self, instance, value):
        super(CameraPreview, self).on_collapse(instance=instance, value=value)
        if value:
            self.on_leave()
        else:
            self.on_enter()

    def on_enter(self):
        print '{} is opened'.format(self.title)

    def on_leave(self):
        print '{} is closed'.format(self.title)
