import os
from kivy.clock import Clock
from kivy.lang import Builder
from screens.base.base import BaseScreen
from util.config_util import get_config, set_config

Builder.load_file(os.path.join(os.path.dirname(__file__), 'result.kv'))


class ResultScreen(BaseScreen):

    def on_enter(self, *args):
        super(ResultScreen, self).on_enter(*args)
        Clock.schedule_once(self.update_save_path)

    def update_save_path(self, *args):
        self.ids.save_path.text = get_config('orb', 'save_path', os.path.expanduser('~/'))

    def on_save(self):
        print 'Saving stitched image files'

    def on_change_folder_path(self):
        new_path = self.ids.save_path.text
        set_config('orb', 'save_path', new_path)
