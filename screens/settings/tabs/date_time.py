import os

import datetime

from kivy.uix.label import Label
from kivymd.snackbar import Snackbar

import util.common
import util.time_util
from kivy.clock import Clock
from kivy.config import _is_rpi
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from screens.settings.tabs.base import BaseTab
from widgets.datetime.calendar.calendar_ui import DatePicker
from widgets.datetime.time_picker import TimePicker
from widgets.spinner import ORBSpinner

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'datetime.kv'))


class DateTimeTab(BaseTab):

    date_picker = None
    time_picker = None
    tz_spinner = None
    index = 3
    has_picker = False

    def __init__(self, **kwargs):
        super(DateTimeTab, self).__init__(**kwargs)

    def on_enter_tab(self, *args):
        if not self.has_picker:
            self.has_picker = True
            Clock.schedule_once(self.add_pickers)
            Clock.schedule_interval(self.update_time, 60)

    def add_pickers(self, *args):
        self.date_picker = DatePicker(size_hint=(None, None), width=120, height=40)
        self.time_picker = TimePicker(size_hint=(None, None), width=65, height=40)
        self.tz_spinner = ORBSpinner(key='timezone', size_hint_x=.5, pos_hint={'center_y': .65})
        # s_time = time.time()
        self.ids.lb_manual_container.add_widget(self.date_picker)
        self.ids.lb_manual_container.add_widget(Label(text="_", size_hint=(None, None), width=20,
                                                      pos_hint={'center_y': .8}))
        self.ids.lb_manual_container.add_widget(self.time_picker)
        self.ids.spinner_container.add_widget(self.tz_spinner)
        self.set_timezone()
        self.update_time()
        self.ids.ly_root.remove_widget(self.ids.ly_loading)
        self.ids.ly_main.opacity = 1

    def on_leave(self, *args):
        Clock.unschedule(self.update_time)

    def update_time(self, *args):
        local_time = util.time_util.get_local_time()
        self.time_picker.set_value(local_time.strftime("%I:%M"))

    def set_manual_datetime(self):
        str_date = self.date_picker.text
        str_time = self.time_picker.text
        local_dt = datetime.datetime.strptime('{} {}'.format(str_date, str_time), "%m/%d/%Y %H:%M")
        utc_dt = util.time_util.to_utc_datetime(local_dt)
        str_new_time = utc_dt.strftime('%Y-%m-%d %H:%M:%S')
        if _is_rpi:
            os.system("date --set='{}'".format(str_new_time))
        else:
            print 'Setting new UTC date & time: {}'.format(str_new_time)

    def on_sync_btn(self):
        tz = self.tz_spinner.get_value()['timezone']
        util.time_util.set_current_timezone(tz)
        Snackbar(text='Timezone is set as {}'.format(tz)).show()
        self.update_time()
        # if _is_rpi:
        #     Clock.schedule_once(util.common.sync_with_internet_time)
        # else:
        #     pass

    def set_timezone(self, *args):
        tz_list = util.time_util.timezone_list
        self.tz_spinner.set_values(tz_list)
        cur_tz = util.time_util.get_current_timezone()
        if cur_tz in tz_list:
            self.tz_spinner.update_current_value(cur_tz)
