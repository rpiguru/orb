import os
from abc import abstractmethod
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))


class BaseTab(Screen):

    screen_widget = ObjectProperty(None)
    index = NumericProperty(0)

    def on_enter(self, *args):
        Clock.schedule_once(self.on_enter_tab)

    @abstractmethod
    def on_enter_tab(self, *args):
        """
        Inherit this function and customize this if you need to do something when entering to tab...
        DO NOT use `on_enter` function directly in each tab!!!
        """
        pass

    @staticmethod
    def get_app():
        return App.get_running_app()

    def _switch_screen(self, screen_name, direction=None, *args):
        self.get_app().switch_screen(screen_name, direction)

    def widgets(self, container=None):
        if container is None:
            container = self
        for widget in container.walk(restrict=True):
            if hasattr(widget, 'key'):
                if widget.key:
                    yield widget

    def collect_data(self, container=None):
        """
        Walk through child widgets and collect data from inputs
        :return: dict
        """
        data = {}
        for widget in self.widgets(container):
            if not widget.disabled:
                data.update(widget.get_value())

        return data

    def _validate_fields(self, container=None):
        """
        Walk through screen input widgets (inherits from widgets.default.DefaultInput)
        and collect errors
        Returns a dict of field key and errors list similar to the server response
        :return: dict
        """
        result = dict()
        for widget in self.widgets(container):
            result.update(widget.validate())

        return result

    def is_valid(self, container=None):
        """
        Collect fields errors and show if any.
        :return: bool
        """
        self.clear_errors(container)
        errors = self._validate_fields(container)
        if errors:
            return False

        return True
