import os

from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import BooleanProperty, StringProperty
from kivy.uix.screenmanager import Screen
from functools import partial

from widgets.base import DefaultInput

Builder.load_file(os.path.join(os.path.dirname(__file__), 'base.kv'))


class BaseScreen(Screen):

    title = StringProperty('')

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)

    def on_back(self, *args):
        self.switch_screen('menu_screen', 'right')

    def on_exit(self):
        # ExitPopup().open()
        pass

    def on_touch_down(self, touch):
        """
        Reset the inactivity timer.
        Do not forget to call super when overriding this method.
        :param touch:
        :return:
        """
        super(BaseScreen, self).on_touch_down(touch)

    def on_leave(self, *args):
        pass

    @staticmethod
    def get_app():
        return App.get_running_app()

    def switch_screen(self, screen_name, direction=None):
        # self.hide_widgets()
        Clock.schedule_once(partial(self._switch_screen, screen_name, direction))

    def go_to_settings(self, tab_name):
        self.hide_widgets()
        Clock.schedule_once(partial(self._go_to_settings, tab_name))

    def _go_to_settings(self, tab_name, *args):
        self.get_app().go_to_settings(tab_name)

    def _switch_screen(self, screen_name, direction=None, *args):
        self.get_app().switch_screen(screen_name, direction)

    def widgets(self, container=None):
        if container is None:
            container = self
        for widget in container.walk(restrict=True):
            if hasattr(widget, 'key'):
                if widget.key:
                    yield widget

    def collect_data(self, container=None):
        """
        Walk through child widgets and collect data from inputs
        :return: dict
        """
        data = {}
        for widget in self.widgets(container):
            if not widget.disabled:
                data.update(widget.get_value())

        return data

    def set_data(self, data, container=None):
        """
        Walk through child widgets and set inputs
        :return: dict
        """
        pass
        # for widget in self.widgets(container):
        #     if isinstance(widget, DefaultInput):
        #         val = data.get(widget.key, '')
        #         widget.set_value(val)

    def _validate_fields(self, container=None):
        """
        Walk through screen input widgets (inherits from widgets.default.DefaultInput)
        and collect errors
        Returns a dict of field key and errors list similar to the server response
        :return: dict
        """
        result = dict()
        for widget in self.widgets(container):
            result.update(widget.validate())

        return result

    def show_errors(self, data, container=None):
        """
        Display response errors on the screen input widgets
        :param container:
        :param data: dict
        :return:
        """
        for widget in self.widgets(container):
            error = data.get(widget.key)
            if error:
                if hasattr(widget, 'show_error'):
                    if isinstance(data[widget.key], list):
                        widget.show_error(data[widget.key][0])
                    else:
                        widget.show_error(str(data[widget.key]))

    def clear_errors(self, container=None):
        for widget in self.widgets(container):
            widget.clear_errors()

    def is_valid(self, container=None):
        """
        Collect fields errors and show if any.
        :return: bool
        """
        self.clear_errors(container)
        errors = self._validate_fields(container)
        if errors:
            self.show_errors(errors, container)
            return False

        return True

    @property
    def animated_widgets(self):
        for widget in self.walk(restrict=True):
            if hasattr(widget, 'appear') and hasattr(widget, 'reset_widget'):
                yield widget

    def reset_widgets(self):
        for widget in self.animated_widgets:
            widget.reset_widget()

    def hide_widgets(self, *args):
        for widget in self.animated_widgets:
            widget.hide()

    def show_widgets(self, *args):
        for widget in self.animated_widgets:
            widget.appear()

    def on_pre_enter(self, *args):
        self.reset_widgets()

    def on_enter(self, *args):
        self.show_widgets()
