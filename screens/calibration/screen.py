import os
from functools import partial

from kivy.clock import Clock
from kivy.lang import Builder
from kivymd.button import MDRaisedButton
from screens.base.base import BaseScreen
from widgets.button import ORBButton, WhiteButton
from widgets.label import H3

Builder.load_file(os.path.join(os.path.dirname(__file__), 'calibration.kv'))


class CalibrationScreen(BaseScreen):

    selected_btn = None

    def on_enter(self, *args):
        super(CalibrationScreen, self).on_enter(*args)
        Clock.schedule_once(self.add_buttons)

    def add_buttons(self, *args):
        container = self.ids.container
        for i in range(1, 25):
            container.add_widget(H3(text='P{}'.format(i), background_color=(1, 0, 1, 1), color=(.1, .1, .1, 1)))
        for i in range(1, 6):
            for j in range(1, 25):
                _btn = ORBButton(text='C{}'.format(i))
                _btn.button_type = 'white'
                _btn.bind(on_release=partial(self.on_btn, j, i))
                container.add_widget(_btn)

    def on_btn(self, rpi_num, camera_num, *args):
        btn = args[0]
        if self.selected_btn is not None:
            self.selected_btn.set_white_color()
        btn.set_default_color()
        self.selected_btn = btn
        print 'RPi: {}, Camera: {}'.format(rpi_num, camera_num)
