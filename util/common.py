"""
    Common utilities
"""
import os
import time
from kivy.config import _is_rpi
from util import config_util
import socket


def get_current_version():
    return "0.0.1"


def get_new_version():
    # TODO: get the latest version
    return '0.2.0'


def turn_bright(n):
    """
    Adjust brightness of touch screen
    :param n: brightness value, range 0 ~ 255
    :return:
    """
    if n < 0:
        n = 0
    if n > 255:
        n = 255
    if _is_rpi:
        os.system('echo {} > /sys/class/backlight/rpi_backlight/brightness'.format(n))


def sync_with_internet_time(*args):
    if _is_rpi:
        os.system('sudo /etc/init.d/ntp stop')
        os.system('sudo ntpd -q -g')
        time.sleep(3)
        os.system('sudo /etc/init.d/ntp start')


def get_name():
    return config_util.get_config('general', 'device_name', '')


def set_name(val):
    config_util.set_config('general', 'device_name', val)


def get_title():
    return config_util.get_config('general', 'title', 'ORB')


def get_hostname():
    return socket.gethostname()


def get_screen_saver_time():
    return int(config_util.get_config('general', 'screen_saver_time', '300')) * 60


def get_serial_number():
    """
    Get Serial Number of RPi
    """
    sn = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                sn = line[10:26]
        f.close()
    except IndexError:
        sn = "ERROR000000000"

    return sn


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])
