"""
    Utility for date & time operations
"""
import glob
import config_util
import datetime
from kivy.config import _is_rpi
import pytz


def get_current_timezone():
    """
    Get Olson timezone name from ini file
    :return:
    """
    return config_util.get_config('general', 'timezone', 'Europe/Amsterdam')


def set_current_timezone(tz):
    """
    Save new timezone value to ini file
    :param tz:
    :return:
    """
    return config_util.set_config('general', 'timezone', tz)


def get_local_time():
    """
    Get local time based on the timezone setting in `orb.ini`
    :return:
    """
    str_tz = get_current_timezone()
    utc_time = datetime.datetime.utcnow()
    if str_tz is None:
        local_time = utc_time
    else:
        tz = pytz.timezone(str_tz)
        local_time = pytz.utc.localize(utc_time, is_dst=None).astimezone(tz)
    return local_time


def to_utc_datetime(local_dt):
    """
    Convert local datetime to utc datetime
    """
    tz = pytz.timezone(get_current_timezone())
    return tz.normalize(tz.localize(local_dt)).astimezone(pytz.utc)


def get_all_timezone_names():
    """
    Get Olson timezone names
    :return: List of Olson timezone names
    """
    # FIXME: If we need additional timezones out of US/Europe, just add!
    continental_list = ['US', 'Europe']

    zone_list = []
    if _is_rpi:
        for cont in continental_list:
            for zone in glob.glob('/usr/share/zoneinfo/{}/*'.format(cont)):
                zone_list.append('{}/{}'.format(*(zone.split('/')[-2:])))
    else:
        zone_list = ['US/Aleutian', 'US/Arizona', 'US/Indiana-Starke', 'US/Mountain', 'US/Pacific', 'US/Samoa',
                     'US/Hawaii', 'US/Central', 'US/Eastern', 'US/Alaska', 'US/East-Indiana', 'US/Michigan',
                     'US/Pacific-New', 'Europe/Podgorica', 'Europe/Oslo', 'Europe/Stockholm', 'Europe/Tiraspol',
                     'Europe/Bucharest', 'Europe/Mariehamn', 'Europe/Nicosia', 'Europe/Paris', 'Europe/Amsterdam',
                     'Europe/Belgrade', 'Europe/Malta', 'Europe/Lisbon', 'Europe/Budapest', 'Europe/Kiev',
                     'Europe/London', 'Europe/Belfast', 'Europe/Monaco', 'Europe/Vatican', 'Europe/Warsaw',
                     'Europe/Gibraltar', 'Europe/Istanbul', 'Europe/Astrakhan', 'Europe/Jersey', 'Europe/Andorra',
                     'Europe/Uzhgorod', 'Europe/Madrid', 'Europe/Moscow', 'Europe/Isle_of_Man', 'Europe/Chisinau',
                     'Europe/Vaduz', 'Europe/San_Marino', 'Europe/Zaporozhye', 'Europe/Vilnius', 'Europe/Simferopol',
                     'Europe/Vienna', 'Europe/Tallinn', 'Europe/Guernsey', 'Europe/Athens', 'Europe/Zurich',
                     'Europe/Helsinki', 'Europe/Ulyanovsk', 'Europe/Zagreb', 'Europe/Bratislava', 'Europe/Saratov',
                     'Europe/Dublin', 'Europe/Sarajevo', 'Europe/Berlin', 'Europe/Skopje', 'Europe/Luxembourg',
                     'Europe/Prague', 'Europe/Riga', 'Europe/Sofia', 'Europe/Ljubljana', 'Europe/Copenhagen',
                     'Europe/Tirane', 'Europe/Rome', 'Europe/Minsk', 'Europe/Kirov', 'Europe/Kaliningrad',
                     'Europe/Samara', 'Europe/Brussels', 'Europe/Volgograd', 'Europe/Busingen']

    # Ignore timezone that is not supported by `pytz` package.
    tz_list = []
    for tz in zone_list:
        try:
            pytz.timezone(tz)
            tz_list.append(tz)
        except pytz.UnknownTimeZoneError:
            pass
    return tz_list


timezone_list = get_all_timezone_names()


if __name__ == '__main__':

    print get_all_timezone_names()
